/**
 * Introduction to SDL 2.0
 * by Gianluca Alloisio a.k.a. theGiallo
 * --------------------------------------------------------------------------------
 *
 * = Lecture 0 =
 * 
 * 
 * Prerequisites:
 *   1- function calling
 *   2- carthesian coordinate system
 * 
 * Objectives:
 *   1- draw lines, points and rectangles
 *   2- fill rectangles
 *   3- clear screen
 *   4- change color
 * 
 * Tasks:
 * 
 * Try to experiment with other draw functions like
 *     SDL_RenderDrawLine
 *     SDL_RenderDrawLines
 *     SDL_RenderDrawPoint
 *     SDL_RenderDrawPoints
 *     SDL_RenderDrawRect
 *     SDL_RenderDrawRects
 *     SDL_RenderFillRect
 *     SDL_RenderFillRects
 *     
 * You can find documentation links in the "Related Functions" section of http://wiki.libsdl.org/SDL_RenderDrawLine
 *     
 * For example:
 *
 *     SDL_Rect rect;
 *     
 *     rect.x = 100;
 *     rect.y = 200;
 *     rect.w = 400;
 *     rect.h = 300;
 *     
 *     res = SDL_RenderDrawRect(
 *                        renderer, // SDL_Renderer* renderer: the renderer to affect
 *                        &rect     // an SDL_Rect structure representing the rectangle to draw, or NULL to outline the entire rendering target
 *           );
 **/

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <iostream>


int main(int argc, char** argv)
{
	SDL_Window *window; // Declare a pointer to an SDL_Window
	SDL_Renderer *renderer = NULL;
	int ret;

	// Initialize
	//---

	// Initialize SDL's Video subsystem
	ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			std::cerr<<"SDL Error: "<< error <<" at line #"<<__LINE__<<" of file "<<__FILE__<<std::endl;
//			SDL_ClearError();
			return 1;
		}
	}

	// Create an application window with the following settings:
	window = SDL_CreateWindow(
						"SDL2 LibreLab Workshop - Lecture 0", // const char* title
						SDL_WINDOWPOS_UNDEFINED, 			  // int x: initial x position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						SDL_WINDOWPOS_UNDEFINED, 			  // int y: initial y position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						640, 								  // int w: width, in pixels
						480, 							      // int h: height, in pixels
						SDL_WINDOW_SHOWN 					  // Uint32 flags: window options, see docs
			);

	// Check that the window was successfully made
	if(window==NULL)
	{
		// In the event that the window could not be made...
		std::cerr << "Could not create window: "<<SDL_GetError()<<" on line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
		SDL_Quit();
		return 1;
	}

	// Create accelerated renderer
	renderer = SDL_CreateRenderer(
								window,						// SDL_Window* window: the window where rendering is displayed
								-1,							// int index: the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
								SDL_RENDERER_ACCELERATED);	// Uint32 flags: 0, or one or more SDL_RendererFlags OR'd together;


	// Program life cycle
	//---

	// set the color to draw with
	SDL_SetRenderDrawColor(
						renderer,	// SDL_Renderer* renderer: the renderer to affect
						255,		// Uint8 r: Red
						0,			// Uint8 g: Green
						0,			// Uint8 b: Blue
						255);		// Uint8 a: Alpha

	// draw a line
	ret = SDL_RenderDrawLine(
						renderer,	// SDL_Renderer* renderer: the renderer in which draw
						10,			// int x1: x of the starting point
						10,			// int y1: y of the starting point
						100,		// int xy: x of the end point
						100);		// int y2: y of the end point
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			std::cerr<<"Could not draw a line SDL Error: "<< error <<" at line #"<<__LINE__<<" of file "<<__FILE__<<std::endl;
//			SDL_ClearError();
			return 1;
		}
	}

	// update the screen with the performed rendering
    SDL_RenderPresent(renderer);

	// Wait for 3000 milliseconds
	SDL_Delay(3000);


	// set the color to draw with
	SDL_SetRenderDrawColor(
						renderer,	// SDL_Renderer* renderer: the renderer to affect
						0,			// Uint8 r: Red
						128,		// Uint8 g: Green
						128,		// Uint8 b: Blue
						255);		// Uint8 a: Alpha

    // Clear the entire screen to our selected color.
    SDL_RenderClear(renderer);

	// update the screen with the performed rendering
    SDL_RenderPresent(renderer);

	// Wait for 3000 milliseconds
	SDL_Delay(3000);



	// Exit
	//---

	// Destroy renderer
    SDL_DestroyRenderer(renderer);

	// Close and destroy the window
	SDL_DestroyWindow(window);

	SDL_Quit();
	return 0;
}
