/**
 * Introduction to SDL 2.0
 * by Gianluca Alloisio a.k.a. theGiallo
 * --------------------------------------------------------------------------------
 *
 * = Lecture 5 =
 * 
 * 
 * Prerequisites:
 *  1- function calling
 *  2- carthesian coordinate system
 *  3- draw lines, points and rectangles
 *  4- fill rectangles
 *  5- clear screen
 *  6- change color
 *  7- concept of vectorial velocity
 *  8- game loop
 * 
 * Objectives:
 *  1- load ttf fonts
 *  2- render text with custom settings
 * 
 * Tasks:
 * 
 *  0- look at the present code that shows you most of SDL_ttf and that renders the "Hello world!"s
 *  1- render the infos printed each frame at the top left corner of the screen
 *  2- make a function that correctly renders a given text with breaklines at a given position with a given font
 *  3- use the getTimeInSec to calc the time duration of each different text render call with the same text and render them
 *  4- make a function that creates (only once) a buffer with the ASCII characters textures and uses them to render a given text (use TTF_RenderGlyph_* to create the buffer)
 *  5- make a function like the above one that uses a texture atlas instead of a buffer
 * 
 **/

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL_ttf.h>
#include <iostream>
#include <cmath>
#include <utility> // for std::move

const double MAX_FPS = 120.0;
const double MIN_FPS = 16.0;
const double EXPECTED_FPS = 60.0;
const double MAX_UPS = 300.0;
const double FIXED_TIMESTEP = 1.0 / MAX_UPS;
const double FIXED_TIMESTEP_2 = FIXED_TIMESTEP * FIXED_TIMESTEP;
const double MIN_TIME = 1.0 / MAX_FPS;
const double MAX_TIME = 1.0 / MIN_FPS;

// If you know templates or you want to give them a try/look
//---
template <typename T>
void linearInterpolationV(unsigned int length, T start[], T end[], T result[], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=length; i++)
    {
        result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
    }
}
// if you have a known at compile time length this is better
template<typename T, unsigned int length>
inline void linearInterpolationV(T start[], T end[], T result[], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
    {
        result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
    }
}
// use this with integers numbers
template<typename T, unsigned int length>
inline void linearInterpolationRoundV(T start[], T end[], T result[], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
    {
        result[i] = round(static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha);
    }
}

// If you don't know templates. These functions do the same as template<typename T, unsigned int length> linearInterpolationV( ...
//---
inline void linearInterpolationf4(float start[4], float end[4], float result[4], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=4; i++) // hopefully this loop will be unrolled
    {
        result[i] = start[i]*alpha + end[i]*omalpha;
    }
}
inline void linearInterpolationf3(float start[3], float end[3], float result[3], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=3; i++) // hopefully this loop will be unrolled
    {
        result[i] = start[i]*alpha + end[i]*omalpha;
    }
}
inline void linearInterpolationf2(float start[2], float end[2], float result[2], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=2; i++) // hopefully this loop will be unrolled
    {
        result[i] = start[i]*alpha + end[i]*omalpha;
    }
}
inline void linearInterpolationf(float start, float end, float result, float alpha)
{
    float omalpha = 1 - alpha;
    result = start*alpha + end*omalpha;
}


struct Color
{
    Uint8 r,g,b,a;
    Color(Uint8 r=255,Uint8 g=255,Uint8 b=255,Uint8 a=255) : r(r),g(g),b(b),a(a){}
    void makeInterpolationOf(Color& a, Color& b, float alpha)
    {
        linearInterpolationRoundV<Uint8,4>(static_cast<Uint8*>(&a.r),static_cast<Uint8*>(&b.r),static_cast<Uint8*>(&r),alpha);
    }
};
struct PixPos
{
    int x,y;
    PixPos(int x=0, int y=0) : x(x),y(y){}
    void makeInterpolationOf(PixPos& a, PixPos& b, float alpha)
    {
        linearInterpolationRoundV<int,2>(static_cast<int*>(&a.x),static_cast<int*>(&b.x),static_cast<int*>(&x),alpha);
    }
};
struct Aspect
{
    Color color;
    PixPos pixel_position;
    Aspect(){};
    Aspect(Color&& _color, PixPos&& _pixel_position) : color(std::move(_color)), pixel_position(std::move(_pixel_position)) {};
    void makeInterpolationOf(Aspect& a, Aspect& b, float alpha)
    {
        color.makeInterpolationOf(a.color, b.color, alpha);
        pixel_position.makeInterpolationOf(a.pixel_position, b.pixel_position, alpha);
    }
};
struct PhysicsProperties
{
    struct Pos {float x,y;} pos;
    struct Vel {float x,y;} vel;
    struct Acc {float x,y;} acc;
    PhysicsProperties() : pos({0,0}), vel({0,0}), acc({0,0}){}
    PhysicsProperties(Pos& pos, Vel& vel, Acc& acc) : pos(pos), vel(vel), acc(acc){}
    PhysicsProperties(Pos&& _pos, Vel&& _vel, Acc&& _acc) : pos(std::move(_pos)), vel(std::move(_vel)), acc(std::move(_acc)){}
    void makeInterpolationOf(PhysicsProperties& a, PhysicsProperties& b, float alpha)
    {
        linearInterpolationV<float,4>(static_cast<float*>(&a.pos.x),static_cast<float*>(&b.pos.x),static_cast<float*>(&pos.x),alpha);
    }
    float totalAcc(Uint8 index)
    {
        // here we will calculate the total acceleration affecting the object; for example gravity, for now nothing.
        // (this probably could be function of pos, if so we have to separate the for cycle in stepAhead(...) )
        return 0;
    }
    // leapfrog integration
    void stepAhead(PhysicsProperties& prev_state)
    {
        // separate this cycle if totalAcc is in function of pos
        for (Uint8 i=0; i!=2; i++)
        {
            (&pos.x)[i] = (&prev_state.pos.x)[i] + (&prev_state.vel.x)[i]*FIXED_TIMESTEP + 0.5f*(&prev_state.acc.x)[i]*FIXED_TIMESTEP_2;
            (&acc.x)[i] = totalAcc(i);
            (&vel.x)[i] = (&prev_state.vel.x)[i] + 0.5f*((&acc.x)[i]+(&prev_state.acc.x)[i])*FIXED_TIMESTEP;
        }
    }
};

// an example of a working structure using the above ones
struct Square
{
    PhysicsProperties pp[3];
    Aspect aspect; // if aspect is in function of time or it's static we don't have to interpolate
    int px_side;
    // you should not touch these, so they should be made private and accessed through a method
    Uint8 currID;
    Uint8 prevID;
    const Uint8 renderID = 2;

    Square() : px_side(10), currID(0), prevID(1){}

    void fixedTSUpdate(double game_time)
    {
        std::swap(currID, prevID);
        pp[currID].stepAhead(pp[prevID]);
    }

    void smoothToRenderState(float alpha)
    {
        pp[renderID].makeInterpolationOf(pp[currID],pp[prevID],alpha);
    }

    void render(SDL_Renderer* renderer)
    {
        for (Uint8 i=0; i!=2; i++)
        {
            (&aspect.pixel_position.x)[i] = std::round((&pp[renderID].pos.x)[i]);
        }

        // Declare and initialize our rectangle
        SDL_Rect rect = {
                        aspect.pixel_position.x, aspect.pixel_position.y,   // int x,y: position of the top left corner
                        px_side, px_side};                                  // int w,h: width and height

        // set the color to draw with
        SDL_SetRenderDrawColor(
                            renderer,        // SDL_Renderer* renderer: the renderer to affect
                            aspect.color.r,  // Uint8 r: Red
                            aspect.color.g,  // Uint8 g: Green
                            aspect.color.b,  // Uint8 b: Blue
                            aspect.color.a); // Uint8 a: Alpha

        int ret;
        ret = SDL_RenderFillRect(
                                renderer,    // SDL_Renderer*   renderer: the renderer to affect
                                   &rect);   // const SDL_Rect* rect: the rectangle to fill);
        if (ret != 0)
        {
            const char *error = SDL_GetError();
            if (*error != '\0')
            {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not draw a rect. SDL Error: %s at line #%d of file %s", error, __LINE__, __FILE__);
                SDL_ClearError();
            }
        }
    }

    void update(double game_time, double delta_time){};
};


double getTimeInSec()
{
    static double coeff;
    static bool isInitialized;
    if (!isInitialized) {
        isInitialized = true;
        Uint64 freq = SDL_GetPerformanceFrequency();
        coeff = 1.0 / (double)freq;
    }
    Uint64 val = SDL_GetPerformanceCounter();

    return (double)val * coeff;
}


int main(int argc, char** argv)
{
    // SDL version
    SDL_version SDL_compiled;
    SDL_version SDL_linked;

    SDL_VERSION(&SDL_compiled);
    SDL_GetVersion(&SDL_linked);
    printf("Compiled against SDL version %d.%d.%d\n",
           SDL_compiled.major, SDL_compiled.minor, SDL_compiled.patch);
    printf("Linking against SDL version %d.%d.%d\n",
           SDL_linked.major, SDL_linked.minor, SDL_linked.patch);

    // SDL_ttf version
    SDL_version SDL_ttf_compiled;
    const SDL_version *SDL_ttf_linked=TTF_Linked_Version();
    SDL_TTF_VERSION(&SDL_ttf_compiled);
    printf("Compiled against SDL_ttf version: %d.%d.%d\n", 
            SDL_ttf_compiled.major, SDL_ttf_compiled.minor, SDL_ttf_compiled.patch);
    printf("Linking against SDL_ttf version: %d.%d.%d\n", 
            SDL_ttf_linked->major, SDL_ttf_linked->minor, SDL_ttf_linked->patch);


#ifdef DEBUG
    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);
#endif

    SDL_Window *window; // Declare a pointer to an SDL_Window
    SDL_Renderer *renderer = NULL;
    int ret;

    // Initialize
    //---

    // Initialize SDL_ttf
    if(TTF_Init()==-1)
    {
        std::cerr<<"TTF_Init: "<<TTF_GetError()<<std::endl;
        return 1;
    }

    // Initialize SDL's Video subsystem
    ret = SDL_Init(SDL_INIT_VIDEO);
    if (ret != 0)
    {
        const char *error = SDL_GetError();
        if (*error != '\0')
        {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not SDL_Init. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
            // SDL_ClearError();
            return 1;
        }
    }

    // Create an application window with the following settings:
    window = SDL_CreateWindow(
                        "SDL2 LibreLab Workshop - Lecture 5",    // const char* title
                        SDL_WINDOWPOS_UNDEFINED,                 // int x: initial x position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
                        SDL_WINDOWPOS_UNDEFINED,                 // int y: initial y position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
                        800,                                     // int w: width, in pixels
                        600,                                     // int h: height, in pixels
                        SDL_WINDOW_SHOWN                         // Uint32 flags: window options, see docs
            );

    // Check that the window was successfully made
    if(window==NULL)
    {
        const char *error = SDL_GetError();
        if (*error != '\0')
        {
            // In the event that the window could not be made...
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create window. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
            SDL_Quit();
            return 1;
        }
    }

    // Create accelerated renderer
    renderer = SDL_CreateRenderer(
                                window,                       // SDL_Window* window: the window where rendering is displayed
                                -1,                           // int index: the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
                                SDL_RENDERER_ACCELERATED);    // Uint32 flags: 0, or one or more SDL_RendererFlags OR'd together;


    // Program life cycle
    //---

    SDL_Event event;
    bool running = true;
    double old_time = getTimeInSec();
    double game_time = 0, alpha, time_accumulator;
    int n_steps;

    Square sq;
    sq.pp[sq.currID].vel.x = 20;
    sq.pp[sq.currID].vel.y = -75;
    
    sq.pp[sq.currID].pos.x = 50;
    sq.pp[sq.currID].pos.y = 300;

    // load font .ttf at size 16 into font
    TTF_Font *font;
    font = TTF_OpenFont("fonts/Inconsolata/Inconsolata-Regular.ttf",    // const char *file: File name to load font from.
                                                                32);    // int ptsize: Point size (based on 72DPI) to load font as. This basically translates to pixel height.
    if(!font)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_OpenFont: %s\n", TTF_GetError());
        return 2;
    }

    // get the loaded font's style
    {
    int style;
    style = TTF_GetFontStyle(font);
    std::cout<<"The font style is:";
    if(style==TTF_STYLE_NORMAL)
        std::cout<<" normal";
    else {
        if(style&TTF_STYLE_BOLD)
            std::cout<<" bold";
        if(style&TTF_STYLE_ITALIC)
            std::cout<<" italic";
        if(style&TTF_STYLE_UNDERLINE)
            std::cout<<" underline";
        if(style&TTF_STYLE_STRIKETHROUGH)
            std::cout<<" strikethrough";
    }
    std::cout<<std::endl;
    }
    
    // set the loaded font's style to bold italics
    TTF_SetFontStyle(font, TTF_STYLE_BOLD|TTF_STYLE_ITALIC);

    // get the loaded font's style
    {
    int style;
    style = TTF_GetFontStyle(font);
    std::cout<<"The font style is:";
    if(style==TTF_STYLE_NORMAL)
        std::cout<<" normal";
    else {
        if(style&TTF_STYLE_BOLD)
            std::cout<<" bold";
        if(style&TTF_STYLE_ITALIC)
            std::cout<<" italic";
        if(style&TTF_STYLE_UNDERLINE)
            std::cout<<" underline";
        if(style&TTF_STYLE_STRIKETHROUGH)
            std::cout<<" strikethrough";
    }
    std::cout<<std::endl;
    }

    {
    // get the loaded font's outline width
    int outline=TTF_GetFontOutline(font);
    SDL_Log("The font outline width is %d pixels\n",outline);
    }
    
    // set the loaded font's outline to 1 pixel wide
    TTF_SetFontOutline(font, 1);
    
    {
    // get the loaded font's outline width
    int outline=TTF_GetFontOutline(font);
    SDL_Log("The font outline width is %d pixels\n",outline);
    }

    {
    // get the loaded font's hinting setting
    int hinting=TTF_GetFontHinting(font);
    SDL_Log("The font hinting is currently set to %s\n",
            hinting==TTF_HINTING_NORMAL?"Normal":
            hinting==TTF_HINTING_LIGHT?"Light":
            hinting==TTF_HINTING_MONO?"Mono":
            hinting==TTF_HINTING_NONE?"None":
            "Unknonwn");
    }

    // set the loaded font's hinting to optimized for monochrome rendering
    TTF_SetFontHinting(font, TTF_HINTING_MONO);
    // set the loaded font's hinting to optimized for monochrome rendering
    TTF_SetFontHinting(font, TTF_HINTING_NORMAL);

    {
    // get the loaded font's kerning setting
    int kerning = TTF_GetFontKerning(font);
    SDL_Log("The font kerning is currently set to %s\n",
            kerning==0?"Off":"On");
    }
    
    // turn off kerning on the loaded font
    TTF_SetFontKerning(font, 0);

    // turn on kerning on the loaded font
    TTF_SetFontKerning(font, 1);

    // get the loaded font's max height
    SDL_Log("The font max height is: %d\n", TTF_FontHeight(font));

    // get the loaded font's max ascent
    /**
     * Get the maximum pixel ascent of all glyphs of the loaded font.
     * This can also be interpreted as the distance from the top of the font to the baseline.
     * It could be used when drawing an individual glyph relative to a top point, by combining it
     * with the glyph's maxy metric to resolve the top of the rectangle used when blitting the glyph on the screen.
     * 
     * rect.y = top + TTF_FontAscent(font) - glyph_metric.maxy;
     **/
    SDL_Log("The font ascent is: %d\n", TTF_FontAscent(font));

    // get the loaded font's max descent
    /**
     * Get the maximum pixel descent of all glyphs of the loaded font.
     * This can also be interpreted as the distance from the baseline to the bottom of the font.
     * It could be used when drawing an individual glyph relative to a bottom point, by combining it
     * with the glyph's maxy metric to resolve the top of the rectangle used when blitting the glyph on the screen.
     *
     * rect.y = bottom - TTF_FontDescent(font) - glyph_metric.maxy;
     **/
    SDL_Log("The font descent is: %d\n", TTF_FontDescent(font));

    // get the loaded font's line skip height
    /**
     * Get the recommended pixel height of a rendered line of text of the loaded font.
     * This is usually larger than the TTF_FontHeight of the font.
     **/
    SDL_Log("The font line skip is: %d\n", TTF_FontLineSkip(font));

    // get the loaded font's number of faces
    SDL_Log("The number of faces in the font is: %ld\n", TTF_FontFaces(font));

    // get the loaded font's face fixed status
    if(TTF_FontFaceIsFixedWidth(font))
        SDL_Log("The font is fixed width.\n");
    else
        SDL_Log("The font is not fixed width.\n");

    {
    // get the loaded font's face name
    char *familyname=TTF_FontFaceFamilyName(font);
    if(familyname)
        SDL_Log("The family name of the face in the font is: %s\n", familyname);
    }

    {
    // get the loaded font's face style name
    char *stylename=TTF_FontFaceStyleName(font);
    if(stylename)
        SDL_Log("The name of the face in the font is: %s\n", stylename);
    }

    {
    // check for a glyph for 'g' in the loaded font
    int index=TTF_GlyphIsProvided(font,'g');
    if(!index)
        SDL_Log("There is no 'g' in the loaded font!\n");
    }

    {
    // get the glyph metric for the letter 'g' in a loaded font
    int minx,maxx,miny,maxy,advance;
    if(TTF_GlyphMetrics(font,'g',&minx,&maxx,&miny,&maxy,&advance)==-1)
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_GlyphMetrics generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    else
    {
        SDL_Log("minx    : %d\n",minx);
        SDL_Log("maxx    : %d\n",maxx);
        SDL_Log("miny    : %d\n",miny);
        SDL_Log("maxy    : %d\n",maxy);
        SDL_Log("advance : %d\n",advance);
    }
    }   

    {
    // get the width and height of a string as it would be rendered in a loaded font
    int w,h;
    if(TTF_SizeText(font,"Hello World!",&w,&h))
    {
        // perhaps print the current TTF_GetError(), the string can't be rendered...
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeText generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    } else
    {
        SDL_Log("width=%d height=%d\n",w,h);
    }
    }

    {
    // get the width and height of a string as it would be rendered in a loaded font
    int w,h;
    if(TTF_SizeUTF8(font,"Hello World!",&w,&h))
    {
        // perhaps print the current TTF_GetError(), the string can't be rendered...
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeUTF8 generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    } else
    {
        SDL_Log("width=%d height=%d\n",w,h);
    }
    }

    {
    // get the width and height of a string as it would be rendered in a loaded font
    int w,h;
    Uint16 text[]={'H','e','l','l','o',' ',
                   'W','o','r','l','d','!',0};
    if(TTF_SizeUNICODE(font,text,&w,&h))
    {
        // perhaps print the current TTF_GetError(), the string can't be rendered...
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeUNICODE generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    } else
    {
        SDL_Log("width=%d height=%d\n",w,h);
    }
    }


    while (running)
    {
        SDL_Log("---------");

        // Calc past spent time
        //---
        
        double current_time = getTimeInSec();
        double frame_duration = current_time - old_time;
        double clamped_frame_duration = frame_duration;
        SDL_Log("frame_duration %fms", frame_duration*1000);
        SDL_Log("instant freq %fHz", 1.0/frame_duration);
        if (frame_duration > MAX_TIME)
        {
            clamped_frame_duration = MAX_TIME;
        } else
        if (frame_duration < MIN_TIME)
        {
            double ms_to_wait = (MIN_TIME - frame_duration) * 1000.0;
            SDL_Log("sleep for %fms",ms_to_wait);

            // minimum guaranteed sleep time is 9/10ms, SDL knows this but with it we sleep ~0.1ms more (on the machine of the author)
            if (ms_to_wait>=10)
            {
                // sleep for >=10ms
                SDL_Delay(static_cast<int>(ms_to_wait));

                // busy waiting for <1ms
                double start=getTimeInSec(),i;
                int it=0;
                ms_to_wait = std::modf(ms_to_wait,&i);
                // SDL_Log("b.w. %f",ms_to_wait);
                while ((getTimeInSec()-start)*1000.0 < ms_to_wait)
                {
                    // it++;
                };
                // SDL_Log("b.w. for %d iterations",it);
            } else
            {
                //SDL_Delay(ms_to_wait); // try youtself uncommenting whis line and commenting the b.w.
                // busy waiting for max 10ms
                while ((getTimeInSec()-current_time)*1000.0 < ms_to_wait){};
            }
            current_time = getTimeInSec();
            double real_fd = current_time - old_time;
            SDL_Log("real_fd %fms", real_fd*1000);
            SDL_Log("real instant freq %fHz", 1.0/real_fd);
            clamped_frame_duration = MIN_TIME;
        }

        old_time = current_time;
        time_accumulator += clamped_frame_duration;
        SDL_Log("clamped %fms", clamped_frame_duration*1000);
        SDL_Log("time_accumulator %fms", time_accumulator*1000);

        n_steps = static_cast<int>(time_accumulator/FIXED_TIMESTEP);
        SDL_Log("n_steps %d", n_steps);


        // Event management
        //---

        // look for a close event
        while (SDL_PollEvent(&event))
        {
            switch ( event.type )
            {
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    SDL_Log("Window %d closed", event.window.windowID);
                    running=false;
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }


        // Fixed time-step update
        //---

        // busy waiting to simulate some work, remove it <------------------------------------- !!!
        {
            double ms_to_wait = 0.3 , start=getTimeInSec();
            while ((getTimeInSec()-start)*1000.0 < ms_to_wait){};
        }

        if (n_steps > 0)
        {
            time_accumulator -= n_steps*FIXED_TIMESTEP;
            SDL_Log("over %fms", time_accumulator*1000);

            for (int s=0; s!=n_steps; s++)
            {
                /**
                 * for each element
                 * previous_state = current_state
                 * fixedTSUpdate(game_time, FIXED_TIMESTEP)
                 **/

                game_time += FIXED_TIMESTEP;
            }
        }
        SDL_Log("time_accumulator %fms", time_accumulator*1000);

        alpha = time_accumulator / FIXED_TIMESTEP;


        // Time dependant update
        //---

        /**
         * for each element
         * update(game_time, n_steps*FIXED_TIMESTEP)
         **/


        // Smooth
        //---

        /**
         * for each element
         * current_state = current_state * alpha + previous_state * (1.0 - alpha)
         */


        // Render
        //---

        // set the color to draw with
        SDL_SetRenderDrawColor(
                            renderer,    // SDL_Renderer* renderer: the renderer to affect
                            64,          // Uint8 r: Red
                            64,          // Uint8 g: Green
                            64,          // Uint8 b: Blue
                            255);        // Uint8 a: Alpha
        // Clear the entire screen to our selected color.
        SDL_RenderClear(renderer);

        // <Render all the elements here>

        /**
         * These functions render text using a TTF_Font.
         * There are three modes of rendering:
         * 
         * Solid
         *     Quick and Dirty
         *     Create an 8-bit palettized surface and render the given text at fast quality with the given font and color.
         *     The pixel value of 0 is the colorkey, giving a transparent background when blitted. Pixel and colormap value 1
         *     is set to the text foreground color. This allows you to change the color without having to render the text again.
         *     Palette index 0 is of course not drawn when blitted to another surface, since it is the colorkey, and thus
         *     transparent, though its actual color is 255 minus each of the RGB components of the foreground color.
         *     This is the fastest rendering speed of all the rendering modes. This results in no box around the text, but the
         *     text is not as smooth. The resulting surface should blit faster than the Blended one. Use this mode for FPS and
         *     other fast changing updating text displays. 
         * Shaded
         *     Slow and Nice, but with a Solid Box
         *     Create an 8-bit palettized surface and render the given text at high quality with the given font and colors.
         *     The 0 pixel value is background, while other pixels have varying degrees of the foreground color from the
         *     background color. This results in a box of the background color around the text in the foreground color.
         *     The text is antialiased. This will render slower than Solid, but in about the same time as Blended mode.
         *     The resulting surface should blit as fast as Solid, once it is made. Use this when you need nice text, and
         *     can live with a box. 
         * Blended
         *     Slow Slow Slow, but Ultra Nice over another image
         *     Create a 32-bit ARGB surface and render the given text at high quality, using alpha blending to dither the
         *     font with the given color. This results in a surface with alpha transparency, so you don't have a solid colored
         *     box around the text. The text is antialiased. This will render slower than Solid, but in about the same time as
         *     Shaded mode. The resulting surface will blit slower than if you had used Solid or Shaded. Use this when you want
         *     high quality, and the text isn't changing too fast. 
         * 
         *     Solid
         *     3.4.1 TTF_RenderText_Solid       Draw LATIN1 text in solid mode
         *     3.4.2 TTF_RenderUTF8_Solid       Draw UTF8 text in solid mode
         *     3.4.3 TTF_RenderUNICODE_Solid    Draw UNICODE text in solid mode
         *     3.4.4 TTF_RenderGlyph_Solid      Draw a UNICODE glyph in solid mode
         *     Shaded
         *     3.4.5 TTF_RenderText_Shaded      Draw LATIN1 text in shaded mode
         *     3.4.6 TTF_RenderUTF8_Shaded      Draw UTF8 text in shaded mode
         *     3.4.7 TTF_RenderUNICODE_Shaded   Draw UNICODE text in shaded mode
         *     3.4.8 TTF_RenderGlyph_Shaded     Draw a UNICODE glyph in shaded mode
         *     Blended
         *     3.4.9 TTF_RenderText_Blended     Draw LATIN1 text in blended mode
         *     3.4.10 TTF_RenderUTF8_Blended    Draw UTF8 text in blended mode
         *     3.4.11 TTF_RenderUNICODE_Blended Draw UNICODE text in blended mode
         *     3.4.12 TTF_RenderGlyph_Blended   Draw a UNICODE glyph in blended mode
         **/

        // Solid
        {
            // Render some text in solid black to a new surface
            // then create a texture from it
            // then free the text surface
            SDL_Color color={0,0,0};
            SDL_Surface *text_surface;
            SDL_Texture *text_tex;
            char text[1024];
            sprintf(text,"Hello World! %d",SDL_GetTicks());
            if(!(text_surface=TTF_RenderText_Solid(font, text, color)))
            {
                // handle error here, perhaps print TTF_GetError at least
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Solid generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
            } else
            {

                SDL_Rect tex_size = {0,0,0,0};
                SDL_Rect tex_dest;

                tex_size.w = text_surface->w;
                tex_size.h = text_surface->h;
                tex_dest.w = tex_size.w;
                tex_dest.h = tex_size.h;

                {
                    int ww, wh;
                    SDL_GetWindowSize(  window, // SDL_Window* window: the window to query
                                        &ww,    // int*        w: gets the width of the window
                                        &wh);   // int*        h: gets the height of the window

                    tex_dest.x = (ww - tex_dest.w)/2;
                    tex_dest.y = (wh - tex_dest.h)/2-50;
                }


                text_tex = SDL_CreateTextureFromSurface(renderer, text_surface);
                if (!text_tex)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }
                SDL_FreeSurface(text_surface);

                ret =
                SDL_RenderCopy( renderer,    // SDL_Renderer* renderer: the renderer to affect
                                text_tex,    // SDL_Texture* texture: the source texture
                                &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
                                &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
                if (ret != 0)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }

                // Destroy the texture
                SDL_DestroyTexture(text_tex);
            }
        }

        // Shaded
        {
            // Render some text in solid black to a new surface
            // then create a texture from it
            // then free the text surface
            SDL_Color color={0,0,0}, bgcolor={255,255,255};
            SDL_Surface *text_surface;
            SDL_Texture *text_tex;
            char text[1024];
            sprintf(text,"Hello World! %d",SDL_GetTicks());
            if(!(text_surface=TTF_RenderText_Shaded(font, text, color, bgcolor)))
            {
                // handle error here, perhaps print TTF_GetError at least
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Solid generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
            } else
            {

                SDL_Rect tex_size = {0,0,0,0};
                SDL_Rect tex_dest;

                tex_size.w = text_surface->w;
                tex_size.h = text_surface->h;
                tex_dest.w = tex_size.w;
                tex_dest.h = tex_size.h;

                {
                    int ww, wh;
                    SDL_GetWindowSize(  window, // SDL_Window* window: the window to query
                                        &ww,    // int*        w: gets the width of the window
                                        &wh);   // int*        h: gets the height of the window

                    tex_dest.x = (ww - tex_dest.w)/2;
                    tex_dest.y = (wh - tex_dest.h)/2;
                }


                text_tex = SDL_CreateTextureFromSurface(renderer, text_surface);
                if (!text_tex)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }
                SDL_FreeSurface(text_surface);

                ret =
                SDL_RenderCopy( renderer,    // SDL_Renderer* renderer: the renderer to affect
                                text_tex,    // SDL_Texture* texture: the source texture
                                &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
                                &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
                if (ret != 0)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }

                // Destroy the texture
                SDL_DestroyTexture(text_tex);
            }
        }

        // Blended
        {
            // Render some text in solid black to a new surface
            // then create a texture from it
            // then free the text surface
            SDL_Color color={0,0,0};
            SDL_Surface *text_surface;
            SDL_Texture *text_tex;
            char text[1024];
            sprintf(text,"Hello World! %d",SDL_GetTicks());
            if(!(text_surface=TTF_RenderText_Blended(font, text, color)))
            {
                // handle error here, perhaps print TTF_GetError at least
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Solid generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
            } else
            {

                SDL_Rect tex_size = {0,0,0,0};
                SDL_Rect tex_dest;

                tex_size.w = text_surface->w;
                tex_size.h = text_surface->h;
                tex_dest.w = tex_size.w;
                tex_dest.h = tex_size.h;

                {
                    int ww, wh;
                    SDL_GetWindowSize(  window, // SDL_Window* window: the window to query
                                        &ww,    // int*        w: gets the width of the window
                                        &wh);   // int*        h: gets the height of the window

                    tex_dest.x = (ww - tex_dest.w)/2;
                    tex_dest.y = (wh - tex_dest.h)/2+50;
                }


                text_tex = SDL_CreateTextureFromSurface(renderer, text_surface);
                if (!text_tex)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }
                SDL_FreeSurface(text_surface);

                ret =
                SDL_RenderCopy( renderer,    // SDL_Renderer* renderer: the renderer to affect
                                text_tex,    // SDL_Texture* texture: the source texture
                                &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
                                &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
                if (ret != 0)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }

                // Destroy the texture
                SDL_DestroyTexture(text_tex);
            }
        }

        if (ret != 0)
        {
            const char *error = SDL_GetError();
            if (*error != '\0')
            {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                SDL_ClearError();
            }
        }
        
        // update the screen with the performed rendering
        SDL_RenderPresent(renderer);
    }


    // Exit
    //---

    // Destroy renderer
    SDL_DestroyRenderer(renderer);

    // Close and destroy the window
    SDL_DestroyWindow(window);

    // Quit SDL
    SDL_Quit();

    // Close the font
    TTF_CloseFont(font);

    // Quit SDL_ttf
    if (TTF_WasInit())
    {
        TTF_Quit();
    }
    return 0;
}
