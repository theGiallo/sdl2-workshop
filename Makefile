################################################################################
# This is a very basic makefile. Later then we will improve it.
################################################################################

LIB_SDL2=-L"libraries/SDL2/linux/lib64" -Wl,-rpath=./libraries/SDL2/linux/lib64 -lSDL2-2.0
I_SDL2=-I"libraries/SDL2/linux/include"
LIB_SDL2_TTF=-L"libraries/SDL2_ttf/linux/lib64" -Wl,-rpath=./libraries/SDL2_ttf/linux/lib64 -lSDL2_ttf
I_SDL2_TTF=-I"libraries/SDL2_ttf/linux/include" -iquote"libraries/SDL2/linux/include/SDL2"
LIBS=$(LIB_SDL2)
INCLUDES=$(I_SDL2)
CFLAGS= -std=c++11
OPT_RELEASE= -O3
OPT_DEBUG= -O0 -g3
DEBUG_FLAGS= -DDEBUG $(OPT_DEBUG)
RELEASE_FLAGS= $(OPT_RELEASE)

all: release debug
release: SDL2LLWS_lec00 SDL2LLWS_lec01 SDL2LLWS_lec02 SDL2LLWS_lec03 SDL2LLWS_lec04 SDL2LLWS_lec05
debug: SDL2LLWS_lec00_d SDL2LLWS_lec01_d SDL2LLWS_lec02_d SDL2LLWS_lec03_d SDL2LLWS_lec04_d SDL2LLWS_lec05_d

###############
### release ###

SDL2LLWS_lec00: lec00.cpp Makefile
	g++ lec00.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "SDL2LLWS_lec00"

SDL2LLWS_lec01: lec01.cpp Makefile
	g++ lec01.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "SDL2LLWS_lec01"

SDL2LLWS_lec02: lec02.cpp Makefile
	g++ lec02.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "SDL2LLWS_lec02"

SDL2LLWS_lec03: lec03.cpp Makefile
	g++ lec03.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "SDL2LLWS_lec03"

SDL2LLWS_lec04: lec04.cpp Makefile
	g++ lec04.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "SDL2LLWS_lec04"

SDL2LLWS_lec05: lec05.cpp Makefile
	g++ lec05.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "SDL2LLWS_lec05" $(I_SDL2_TTF) $(LIB_SDL2_TTF)

#############
### debug ###

SDL2LLWS_lec00_d: lec00.cpp Makefile
	g++ lec00.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "SDL2LLWS_lec00_d"

SDL2LLWS_lec01_d: lec01.cpp Makefile
	g++ lec01.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "SDL2LLWS_lec01_d"

SDL2LLWS_lec02_d: lec02.cpp Makefile
	g++ lec02.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "SDL2LLWS_lec02_d"

SDL2LLWS_lec03_d: lec03.cpp Makefile
	g++ lec03.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "SDL2LLWS_lec03_d"

SDL2LLWS_lec04_d: lec04.cpp Makefile
	g++ lec04.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "SDL2LLWS_lec04_d"

SDL2LLWS_lec05_d: lec05.cpp Makefile
	g++ lec05.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "SDL2LLWS_lec05_d" $(I_SDL2_TTF) $(LIB_SDL2_TTF)

clean:
	rm SDL2LLWS_lec*

.PHONY: all clean