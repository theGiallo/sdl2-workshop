/**
 * Introduction to SDL 2.0
 * by Gianluca Alloisio a.k.a. theGiallo
 * --------------------------------------------------------------------------------
 *
 * = Lecture 1 =
 * 
 * 
 * Prerequisites:
 *   1- function calling
 *   2- carthesian coordinate system
 *   3- draw lines, points and rectangles
 *   4- fill rectangles
 *   5- clear screen
 *   6- change color
 *   7- read from std::cin
 *   8- concept of vectorial velocity
 * 
 * Objectives:
 *   1- move rendered things
 * 
 * Tasks:
 * 
 *   1- make the rectangle move from (0,400) to (300,400)
 *   2- make the rectangle move from (0,400) to (300,400) in 10s with constant velocity
 *   3- add a green rectangle and make it move from (450,400) to (300,400) in 10s with constant velocity
 *   4- make the green rectangle move with the same velocity of the red one
 *   5- get from the user (std::cin) the target position of the two rectangles and then move them with the same old velocity
 * 
 **/

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <iostream>


int main(int argc, char** argv)
{
	SDL_Window *window; // Declare a pointer to an SDL_Window
	SDL_Renderer *renderer = NULL;
	int ret;

	// Initialize
	//---

	// Initialize SDL's Video subsystem
	ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not SDL_Init. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
//			SDL_ClearError();
			return 1;
		}
	}

	// Create an application window with the following settings:
	window = SDL_CreateWindow(
						"SDL2 LibreLab Workshop - Lecture 1", 	// const char* title
						SDL_WINDOWPOS_UNDEFINED, 				// int x: initial x position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						SDL_WINDOWPOS_UNDEFINED, 				// int y: initial y position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						800, 									// int w: width, in pixels
						600, 									// int h: height, in pixels
						SDL_WINDOW_SHOWN 						// Uint32 flags: window options, see docs
			);

	// Check that the window was successfully made
	if(window==NULL)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			// In the event that the window could not be made...
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create window. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
			SDL_Quit();
			return 1;
		}
	}

	// Create accelerated renderer
	renderer = SDL_CreateRenderer(
								window,						// SDL_Window* window: the window where rendering is displayed
								-1,							// int index: the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
								SDL_RENDERER_ACCELERATED);	// Uint32 flags: 0, or one or more SDL_RendererFlags OR'd together;

	// Declare and initialize our rectangle
	SDL_Rect rect = {
					10, 10, // int x,y: position of the top left corner
					30, 30};// int w,h: width and height


	// Program life cycle
	//---

		// Render the initial state and wait (for learning/debug purpose)
		//---

		// set the color to draw with
		SDL_SetRenderDrawColor(
							renderer,	// SDL_Renderer* renderer: the renderer to affect
							64,			// Uint8 r: Red
							64,			// Uint8 g: Green
							64,			// Uint8 b: Blue
							255);		// Uint8 a: Alpha

	    // Clear the entire screen to our selected color.
	    SDL_RenderClear(renderer);


		// set the color to draw with
		SDL_SetRenderDrawColor(
							renderer,	// SDL_Renderer* renderer: the renderer to affect
							255,		// Uint8 r: Red
							0,			// Uint8 g: Green
							0,			// Uint8 b: Blue
							255);		// Uint8 a: Alpha

		ret = SDL_RenderFillRect(
								renderer,	// SDL_Renderer*   renderer: the renderer to affect
	                       		&rect);		// const SDL_Rect* rect: the rectangle to fill);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				std::cerr<<"Could not draw a rect SDL Error: "<< error <<" at line #"<<__LINE__<<" of file "<<__FILE__<<std::endl;
				// SDL_ClearError();
				return 1;
			}
		}

		// update the screen with the performed rendering
	    SDL_RenderPresent(renderer);

		// Wait for 2s before starting
		SDL_Delay(2000);

	while (rect.x!=370)
	{
		// Move the rectangle
		//---

		rect.x++;
		rect.y++;


		// Clear the screen
		//---

		// set the color to draw with
		SDL_SetRenderDrawColor(
							renderer,	// SDL_Renderer* renderer: the renderer to affect
							64,			// Uint8 r: Red
							64,		// Uint8 g: Green
							64,		// Uint8 b: Blue
							255);		// Uint8 a: Alpha

	    // Clear the entire screen to our selected color.
	    SDL_RenderClear(renderer);


	    // Render the rectangle
	    //---

		// set the color to draw with
		SDL_SetRenderDrawColor(
							renderer,	// SDL_Renderer* renderer: the renderer to affect
							255,		// Uint8 r: Red
							0,			// Uint8 g: Green
							0,			// Uint8 b: Blue
							255);		// Uint8 a: Alpha

		ret = SDL_RenderFillRect(
								renderer,	// SDL_Renderer*   renderer: the renderer to affect
	                       		&rect);		// const SDL_Rect* rect: the rectangle to fill);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				std::cerr<<"Could not draw a rect SDL Error: "<< error <<" at line #"<<__LINE__<<" of file "<<__FILE__<<std::endl;
				// SDL_ClearError();
				return 1;
			}
		}

		// update the screen with the performed rendering
	    SDL_RenderPresent(renderer);


		// Wait for 16ms before another cycle
		SDL_Delay(16);
	}

	// Wait for 2s before closing
	SDL_Delay(2000);


	// Exit
	//---

	// Destroy renderer
    SDL_DestroyRenderer(renderer);

	// Close and destroy the window
	SDL_DestroyWindow(window);

	SDL_Quit();
	return 0;
}
