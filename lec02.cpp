/**
 * Introduction to SDL 2.0
 * by Gianluca Alloisio a.k.a. theGiallo
 * --------------------------------------------------------------------------------
 *
 * = Lecture 2 =
 * 
 * 
 * Prerequisites:
 *   1- function calling
 *   2- carthesian coordinate system
 *   3- draw lines, points and rectangles
 *   4- fill rectangles
 *   5- clear screen
 *   6- change color
 *   7- concept of vectorial velocity
 * 
 * Objectives:
 *   1- understand the game loop
 *   2- organize the management of rendered elements
 * 
 * Tasks:
 * 
 *   0- look at the prints and the code and try to understand what is going on and why
 *   1- put the correct method calls in the correct points and admire the moving square!
 *   2- make the totalAcc return gravity so that the square falls down (maybe declare a global const)
 *   3- make the square change color in function of time (hint: trigonometry comes in handy)
 *   4- make the square change color in function of his position
 *   5- make the square change side length in function of his velocity
 *   6- make the acceleration of the square in function of his position (hint: Newton's law of universal gravitation)
 *   7- add many squares, put them into a vector and use it in each phase
 * 
 **/

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <iostream>
#include <cmath>
#include <utility> // for std::move

const double MAX_FPS = 120.0;
const double MIN_FPS = 16.0;
const double EXPECTED_FPS = 60.0;
const double MAX_UPS = 300.0;
const double FIXED_TIMESTEP = 1.0 / MAX_UPS;
const double FIXED_TIMESTEP_2 = FIXED_TIMESTEP * FIXED_TIMESTEP;
const double MIN_TIME = 1.0 / MAX_FPS;
const double MAX_TIME = 1.0 / MIN_FPS;

// If you know templates or you want to give them a try/look
//---
template <typename T>
void linearInterpolationV(unsigned int length, T start[], T end[], T result[], float alpha)
{
	float omalpha = 1 - alpha;
	for (int i=0; i!=length; i++)
	{
		result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
	}
}
// if you have a known at compile time length this is better
template<typename T, unsigned int length>
inline void linearInterpolationV(T start[], T end[], T result[], float alpha)
{
	float omalpha = 1 - alpha;
	for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
	{
		result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
	}
}
// use this with integers numbers
template<typename T, unsigned int length>
inline void linearInterpolationRoundV(T start[], T end[], T result[], float alpha)
{
	float omalpha = 1 - alpha;
	for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
	{
		result[i] = round(static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha);
	}
}

// If you don't know templates. These functions do the same as template<typename T, unsigned int length> linearInterpolationV( ...
//---
inline void linearInterpolationf4(float start[4], float end[4], float result[4], float alpha)
{
	float omalpha = 1 - alpha;
	for (int i=0; i!=4; i++) // hopefully this loop will be unrolled
	{
		result[i] = start[i]*alpha + end[i]*omalpha;
	}
}
inline void linearInterpolationf3(float start[3], float end[3], float result[3], float alpha)
{
	float omalpha = 1 - alpha;
	for (int i=0; i!=3; i++) // hopefully this loop will be unrolled
	{
		result[i] = start[i]*alpha + end[i]*omalpha;
	}
}
inline void linearInterpolationf2(float start[2], float end[2], float result[2], float alpha)
{
	float omalpha = 1 - alpha;
	for (int i=0; i!=2; i++) // hopefully this loop will be unrolled
	{
		result[i] = start[i]*alpha + end[i]*omalpha;
	}
}
inline void linearInterpolationf(float start, float end, float result, float alpha)
{
	float omalpha = 1 - alpha;
	result = start*alpha + end*omalpha;
}


struct Color
{
	Uint8 r,g,b,a;
	Color(Uint8 r=255,Uint8 g=255,Uint8 b=255,Uint8 a=255) : r(r),g(g),b(b),a(a){}
	void makeInterpolationOf(Color& a, Color& b, float alpha)
	{
		linearInterpolationRoundV<Uint8,4>(static_cast<Uint8*>(&a.r),static_cast<Uint8*>(&b.r),static_cast<Uint8*>(&r),alpha);
	}
};
struct PixPos
{
	int x,y;
	PixPos(int x=0, int y=0) : x(x),y(y){}
	void makeInterpolationOf(PixPos& a, PixPos& b, float alpha)
	{
		linearInterpolationRoundV<int,2>(static_cast<int*>(&a.x),static_cast<int*>(&b.x),static_cast<int*>(&x),alpha);
	}
};
struct Aspect
{
	Color color;
	PixPos pixel_position;
	Aspect(){};
	Aspect(Color&& _color, PixPos&& _pixel_position) : color(std::move(_color)), pixel_position(std::move(_pixel_position)) {};
	void makeInterpolationOf(Aspect& a, Aspect& b, float alpha)
	{
		color.makeInterpolationOf(a.color, b.color, alpha);
		pixel_position.makeInterpolationOf(a.pixel_position, b.pixel_position, alpha);
	}
};
struct PhysicsProperties
{
	struct Pos {float x,y;} pos;
	struct Vel {float x,y;} vel;
	struct Acc {float x,y;} acc;
	PhysicsProperties() : pos({0,0}), vel({0,0}), acc({0,0}){}
	PhysicsProperties(Pos& pos, Vel& vel, Acc& acc) : pos(pos), vel(vel), acc(acc){}
	PhysicsProperties(Pos&& _pos, Vel&& _vel, Acc&& _acc) : pos(std::move(_pos)), vel(std::move(_vel)), acc(std::move(_acc)){}
	void makeInterpolationOf(PhysicsProperties& a, PhysicsProperties& b, float alpha)
	{
		linearInterpolationV<float,4>(static_cast<float*>(&a.pos.x),static_cast<float*>(&b.pos.x),static_cast<float*>(&pos.x),alpha);
	}
	float totalAcc(Uint8 index)
	{
		// here we will calculate the total acceleration affecting the object; for example gravity, for now nothing.
		// (this probably could be function of pos, if so we have to separate the for cycle in stepAhead(...) )
		return 0;
	}
	// leapfrog integration
	void stepAhead(PhysicsProperties& prev_state)
	{
		// separate this cycle if totalAcc is in function of pos
		for (Uint8 i=0; i!=2; i++)
		{
			(&pos.x)[i] = (&prev_state.pos.x)[i] + (&prev_state.vel.x)[i]*FIXED_TIMESTEP + 0.5f*(&prev_state.acc.x)[i]*FIXED_TIMESTEP_2;
			(&acc.x)[i] = totalAcc(i);
			(&vel.x)[i] = (&prev_state.vel.x)[i] + 0.5f*((&acc.x)[i]+(&prev_state.acc.x)[i])*FIXED_TIMESTEP;
		}
	}
};

// an example of a working structure using the above ones
struct Square
{
	PhysicsProperties pp[3];
	Aspect aspect; // if aspect is in function of time or it's static we don't have to interpolate
	int px_side;
	// you should not touch these, so they should be made private and accessed through a method
	Uint8 currID;
	Uint8 prevID;
	const Uint8 renderID = 2;

	Square() : px_side(10), currID(0), prevID(1){}

	void fixedTSUpdate(double game_time)
	{
		std::swap(currID, prevID);
		pp[currID].stepAhead(pp[prevID]);
	}

	void smoothToRenderState(float alpha)
	{
		pp[renderID].makeInterpolationOf(pp[currID],pp[prevID],alpha);
	}

	void render(SDL_Renderer* renderer)
	{
		for (Uint8 i=0; i!=2; i++)
		{
			(&aspect.pixel_position.x)[i] = std::round((&pp[renderID].pos.x)[i]);
		}

		// Declare and initialize our rectangle
		SDL_Rect rect = {
						aspect.pixel_position.x, aspect.pixel_position.y,	// int x,y: position of the top left corner
						px_side, px_side};										// int w,h: width and height

		// set the color to draw with
		SDL_SetRenderDrawColor(
							renderer,				// SDL_Renderer* renderer: the renderer to affect
							aspect.color.r,			// Uint8 r: Red
							aspect.color.g,			// Uint8 g: Green
							aspect.color.b,			// Uint8 b: Blue
							aspect.color.a);		// Uint8 a: Alpha

		int ret;
		ret = SDL_RenderFillRect(
								renderer,	// SDL_Renderer*   renderer: the renderer to affect
	                       		&rect);		// const SDL_Rect* rect: the rectangle to fill);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not draw a rect. SDL Error: %s at line #%d of file %s", error, __LINE__, __FILE__);
				SDL_ClearError();
			}
		}
	}

	void update(double game_time, double delta_time){};
};


double getTimeInSec()
{
    static double coeff;
    static bool isInitialized;
    if (!isInitialized) {
        isInitialized = true;
        Uint64 freq = SDL_GetPerformanceFrequency();
        coeff = 1.0 / (double)freq;
    }
    Uint64 val = SDL_GetPerformanceCounter();

    return (double)val * coeff;
}


int main(int argc, char** argv)
{
	// interpolators test    REMOVE ME <--------------------------------------------------------!!!
	{
	PhysicsProperties pp_a {{0,0}, {0,0}, {10,10}};
	PhysicsProperties pp_b {{10,10}, {10,10}, {0,0}};
	PhysicsProperties pp_i {{3,3}, {3,3}, {3,3}};
	float alpha = 0.25;
	std::cout<<"pp_i:{"<<pp_i.pos.x<<", "<<pp_i.pos.y<<"}, {"<<pp_i.vel.x<<", "<<pp_i.vel.y<<"}, {"<<pp_i.acc.x<<", "<<pp_i.acc.y<<"}"<<std::endl;
	pp_i.makeInterpolationOf(pp_a,pp_b,alpha);
	std::cout<<"pp_i:{"<<pp_i.pos.x<<", "<<pp_i.pos.y<<"}, {"<<pp_i.vel.x<<", "<<pp_i.vel.y<<"}, {"<<pp_i.acc.x<<", "<<pp_i.acc.y<<"}"<<std::endl;
	}
	{
	Aspect a_a {{0,0,0,0}, {10,10}};
	Aspect a_b {{10,10,10,10}, {0,0}};
	Aspect a_i {{3,3,3,3}, {3,3}};
	float alpha = 0.25;
	std::cout<<"a_i:{"<<static_cast<unsigned short>(a_i.color.r)<<", "<<static_cast<unsigned short>(a_i.color.g)<<", "<<static_cast<unsigned short>(a_i.color.b)<<", "<<static_cast<unsigned short>(a_i.color.a)<<"}, {"<<a_i.pixel_position.x<<", "<<a_i.pixel_position.y<<"}"<<std::endl;
	a_i.makeInterpolationOf(a_a,a_b,alpha);
	std::cout<<"a_i:{"<<static_cast<unsigned short>(a_i.color.r)<<", "<<static_cast<unsigned short>(a_i.color.g)<<", "<<static_cast<unsigned short>(a_i.color.b)<<", "<<static_cast<unsigned short>(a_i.color.a)<<"}, {"<<a_i.pixel_position.x<<", "<<a_i.pixel_position.y<<"}"<<std::endl;
	}

	// SDL_Delay(5000);


#ifdef DEBUG
	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);
#endif

	SDL_Window *window; // Declare a pointer to an SDL_Window
	SDL_Renderer *renderer = NULL;
	int ret;

	// Initialize
	//---

	// Initialize SDL's Video subsystem
	ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not SDL_Init. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
			// SDL_ClearError();
			return 1;
		}
	}

	// Create an application window with the following settings:
	window = SDL_CreateWindow(
						"SDL2 LibreLab Workshop - Lecture 2", 	// const char* title
						SDL_WINDOWPOS_UNDEFINED, 				// int x: initial x position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						SDL_WINDOWPOS_UNDEFINED, 				// int y: initial y position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						800, 									// int w: width, in pixels
						600, 									// int h: height, in pixels
						SDL_WINDOW_SHOWN 						// Uint32 flags: window options, see docs
			);

	// Check that the window was successfully made
	if(window==NULL)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			// In the event that the window could not be made...
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create window. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
			SDL_Quit();
			return 1;
		}
	}

	// Create accelerated renderer
	renderer = SDL_CreateRenderer(
								window,						// SDL_Window* window: the window where rendering is displayed
								-1,							// int index: the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
								SDL_RENDERER_ACCELERATED);	// Uint32 flags: 0, or one or more SDL_RendererFlags OR'd together;


	// Program life cycle
	//---

	SDL_Event event;
	bool running = true;
	double old_time = getTimeInSec();
	double game_time = 0, alpha, time_accumulator;
	int n_steps;

	Square sq;
	sq.pp[sq.currID].vel.x = 20;
	sq.pp[sq.currID].vel.y = -75;
	
	sq.pp[sq.currID].pos.x = 50;
	sq.pp[sq.currID].pos.y = 300;

	while (running)
	{
	    SDL_Log("---------");

		// Calc past spent time
		//---
	    
	    double current_time = getTimeInSec();
	    double frame_duration = current_time - old_time;
	    double clamped_frame_duration = frame_duration;
	    SDL_Log("frame_duration %fms", frame_duration*1000);
	    SDL_Log("instant freq %fHz", 1.0/frame_duration);
	    if (frame_duration > MAX_TIME)
	    {
	        clamped_frame_duration = MAX_TIME;
	    } else
	    if (frame_duration < MIN_TIME)
	    {
	    	double ms_to_wait = (MIN_TIME - frame_duration) * 1000.0;
	        SDL_Log("sleep for %fms",ms_to_wait);

	        // minimum guaranteed sleep time is 9/10ms, SDL knows this but with it we sleep ~0.1ms more (on the machine of the author)
	        if (ms_to_wait>=10)
	        {
	        	// sleep for >=10ms
	        	SDL_Delay(static_cast<int>(ms_to_wait));

	        	// busy waiting for <1ms
		    	double start=getTimeInSec(),i;
	        	int it=0;
		    	ms_to_wait = std::modf(ms_to_wait,&i);
        		// SDL_Log("b.w. %f",ms_to_wait);
		    	while ((getTimeInSec()-start)*1000.0 < ms_to_wait)
		    	{
		    		// it++;
		    	};
	        	// SDL_Log("b.w. for %d iterations",it);
	        } else
	        {
	        	//SDL_Delay(ms_to_wait); // try youtself uncommenting whis line and commenting the b.w.
	        	// busy waiting for max 10ms
	        	while ((getTimeInSec()-current_time)*1000.0 < ms_to_wait){};
	        }
	        current_time = getTimeInSec();
	        double real_fd = current_time - old_time;
		    SDL_Log("real_fd %fms", real_fd*1000);
		    SDL_Log("real instant freq %fHz", 1.0/real_fd);
	        clamped_frame_duration = MIN_TIME;
	    }

	    old_time = current_time;
	    time_accumulator += clamped_frame_duration;
	    SDL_Log("clamped %fms", clamped_frame_duration*1000);
	    SDL_Log("time_accumulator %fms", time_accumulator*1000);

	    n_steps = static_cast<int>(time_accumulator/FIXED_TIMESTEP);
	    SDL_Log("n_steps %d", n_steps);


	    // Event management
	    //---

		// look for a close event
        while (SDL_PollEvent(&event))
        {
            switch ( event.type )
		    {
		    case SDL_WINDOWEVENT:
		        switch (event.window.event)
		        {
		        case SDL_WINDOWEVENT_CLOSE:
		            SDL_Log("Window %d closed", event.window.windowID);
		            running=false;
		            break;
	            default:
		            break;
		        }
		        break;
            default:
	            break;
		    }
        }


        // Fixed time-step update
        //---

    	// busy waiting to simulate some work, remove it <------------------------------------- !!!
    	{
	    	double ms_to_wait = 0.3 , start=getTimeInSec();
	    	while ((getTimeInSec()-start)*1000.0 < ms_to_wait){};
	    }

		if (n_steps > 0)
		{
	    	time_accumulator -= n_steps*FIXED_TIMESTEP;
	    	SDL_Log("over %fms", time_accumulator*1000);

		    for (int s=0; s!=n_steps; s++)
		    {
		    	/**
		    	 * for each element
		         * previous_state = current_state
		         * fixedTSUpdate(game_time, FIXED_TIMESTEP)
		         **/

		        game_time += FIXED_TIMESTEP;
		    }
		}
	    SDL_Log("time_accumulator %fms", time_accumulator*1000);

	    alpha = time_accumulator / FIXED_TIMESTEP;


	    // Time dependant update
	    //---

    	/**
    	 * for each element
         * update(game_time, n_steps*FIXED_TIMESTEP)
         **/


	    // Smooth
	    //---

	    /**
	     * for each element
	     * current_state = current_state * alpha + previous_state * (1.0 - alpha)
	     */


        // Render
        //---

		// set the color to draw with
		SDL_SetRenderDrawColor(
							renderer,	// SDL_Renderer* renderer: the renderer to affect
							64,			// Uint8 r: Red
							64,			// Uint8 g: Green
							64,			// Uint8 b: Blue
							255);		// Uint8 a: Alpha
	    // Clear the entire screen to our selected color.
	    SDL_RenderClear(renderer);

		// <Render all the elements here>
		
		// update the screen with the performed rendering
	    SDL_RenderPresent(renderer);
	}


	// Exit
	//---

	// Destroy renderer
    SDL_DestroyRenderer(renderer);

	// Close and destroy the window
	SDL_DestroyWindow(window);

	SDL_Quit();
	return 0;
}
